package com.legacy.lava_monster.entity;

import com.legacy.lava_monster.LavaMonsterConfig;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.util.math.MathHelper;

public class LavaMonsterAttackGoal extends Goal
{
	/// Handy properties for this class.
	public static final int WINDUP = LavaMonsterConfig.attackWindup;
	public static final int SHOT_COUNT = LavaMonsterConfig.attackShots;
	public static final int SPACING = LavaMonsterConfig.attackSpacing;
	public static final int COOLDOWN = LavaMonsterConfig.attackCooldown;

	/// The entity (host) using this AI.
	public LavaMonsterEntity lavaMonster;
	/// The host's target.
	public LivingEntity target;
	/// The host's moveSpeed.
	public double moveSpeed;
	/// Ticks until the host stops after seeing its target.
	public byte pathDelay = 0;
	/// The attack phase the host is currently in.
	public byte attackPhase = 0;

	public LavaMonsterAttackGoal(LavaMonsterEntity entity, double speed)
	{
		lavaMonster = entity;
		moveSpeed = speed;
		// setMutexBits(3);
	}

	/// Whether this AI should run.
	@Override
	public boolean shouldExecute()
	{
		LivingEntity e = lavaMonster.getAttackTarget();
		if (e == null)
			return false;
		target = e;
		return true;
	}

	/// Whether this AI should continue running.
	@Override
	public boolean shouldContinueExecuting()
	{
		return shouldExecute() || !lavaMonster.getNavigator().noPath();
	}

	/// Called when this AI is stopped.
	@Override
	public void resetTask()
	{
		lavaMonster.setAttackTarget(null);
		target = null;
	}

	/// Called each tick while this AI is running.
	@Override
	public void tick()
	{
		float f = (float) (lavaMonster.getPosX() - target.getPosX());
		float f1 = (float) lavaMonster.getPosY() - (float) lavaMonster.getPosY();
		float f2 = (float) (lavaMonster.getPosZ() - target.getPosZ());

		double distance = MathHelper.sqrt(f * f + f1 * f1 + f2 * f2);

		boolean canSee = lavaMonster.getEntitySenses().canSee(target);
		double range = Math.min(35.0D, 17);

		if (canSee)
			pathDelay = (byte) Math.min(20, pathDelay + 1);
		else
			pathDelay = 0;
		if (distance <= range && pathDelay >= 20)
			lavaMonster.getNavigator().clearPath();
		else
			lavaMonster.getNavigator().tryMoveToEntityLiving(target, moveSpeed);
		lavaMonster.getLookController().setLookPositionWithEntity(target, 20.0F, 20.0F);
		if (lavaMonster.attackDelay > 0)
			return;

		if (distance > range || !canSee)
		{
			lavaMonster.attackDelay = 10;
			return;
		}

		if (++attackPhase == 1)
		{
			lavaMonster.attackDelay = WINDUP;
			// lavaMonster.setBurningState(true);
			return;
		}
		lavaMonster.attackEntityWithRangedAttack(target, 0);
		if (attackPhase <= SHOT_COUNT)
			lavaMonster.attackDelay = SPACING;
		else
		{
			lavaMonster.attackDelay = COOLDOWN;
			attackPhase = 0;
			// lavaMonster.setBurningState(false);
		}
	}
}